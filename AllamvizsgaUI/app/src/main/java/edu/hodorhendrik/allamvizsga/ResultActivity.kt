package edu.hodorhendrik.allamvizsga

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView

class ResultActivity : AppCompatActivity() {

    private lateinit var imageView: ImageView
    private lateinit var textView: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_result)
        val button: Button = findViewById(R.id.btnOpenMain)
        button.setOnClickListener {
            startActivity(Intent(this@ResultActivity, MainActivity::class.java))
        }

        imageView = findViewById(R.id.imageView)
        textView = findViewById(R.id.textView)


        selectContent()
    }

    private fun selectContent() {
        val number = (1..13).random()

        when(number) {
            1 -> {
                imageView.setImageResource(R.drawable.csikosbuzosborz)
                textView.text = "Csikos Büzösborz"
            }
            2 -> {
                imageView.setImageResource(R.drawable.eszakioposzum)
                textView.text = "Északi Oposzum"
            }
            3 -> {
                imageView.setImageResource(R.drawable.feketemedve)
                textView.text = "Fekete Medve"
            }
            4 -> {
                imageView.setImageResource(R.drawable.feherfarkuszarvas)
                textView.text = "Feherfarku Szarvas"
            }
            5 -> {
                imageView.setImageResource(R.drawable.kamadaividra)
                textView.text = "Kanadai Vidra"
            }
            6 -> {
                imageView.setImageResource(R.drawable.kanadaihod)
                textView.text = "Kanadai Hód"
            }
            7 -> {
                imageView.setImageResource(R.drawable.mosomedve)
                textView.text = "Mosomedve"
            }
            8 -> {
                imageView.setImageResource(R.drawable.keletisz_rkemokus)
                textView.text = "Keleti Szürkemokus"
            }
            9 -> {
                imageView.setImageResource(R.drawable.prerifarkas)
                textView.text = "Prérifarkas"
            }
            10 -> {
                imageView.setImageResource(R.drawable.vaddiszn_)
                textView.text = "Vaddisznó"
            }
            11 -> {
                imageView.setImageResource(R.drawable.vadpulyka)
                textView.text = "Vadpulyka"
            }
            12 -> {
                imageView.setImageResource(R.drawable.voroshiuz)
                textView.text = "Vöröshiuz"
            }
            else -> {
                imageView.setImageResource(R.drawable.ic_launcher_foreground)
                textView.text = "?"
            }

        }

    }

}